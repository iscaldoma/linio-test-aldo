<?php
/*
For unit tests execute the next command:
/vendor/bin/phpunit --bootstrap vendor/autoload.php tests/unitTests
*/

class Test {
    //Variables de control para poder delimitar el inicio y el fin de la numeración
    //Control Variables from limit the start and end of numbers
    var $init = 1;
    var $end = 100;

    function makeTest() {
        //Realice un array con los valores de cada pivote para poder optimizar las condicionales, utilice el 15 como pivote tambien ya que es un común denominador de 3 y 5
        //Make an array with values of each pivot to optimization of condicionals, I used the 15 number like pivot because this is common denominator from 3 and 5
        $pivots = array('15'=>'Linianos', '5'=>'IT', '3'=>'Linio');
        for ($i=$this->init; $i<=$this->end; $i++) {
            echo $this->checkNumber($i, $pivots);
        }
        return true;
    }

    function checkNumber($number, $pivots) {
        foreach ($pivots AS $key => $value) {
            if ($number%$key == 0) {
                return $value."<br>";
            }
        }
        return $number."<br>";
    }

    function getInit() {
        return $this->init;
    }

    function getEnd() {
        return $this->end;
    }

    function setInit($init) {
        $this->init =  $init;
    }

    function setEnd($end) {
        $this->end = $end;
    }
}

$test = new Test();
$test->setInit(1);
$test->setEnd(100);
echo $test->makeTest();